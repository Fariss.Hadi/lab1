package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    Random rand= new Random();
    
    public void run() {
        String keepplaying = "y";
            while (keepplaying.equals("y")) {
                System.out.printf("Let's play round %d\n", roundCounter);

                String p_choice = readInput("Your choice (Rock/Paper/Scissors)?");
                if (!rpsChoices.contains(p_choice)) {
                    System.out.printf("I do not understand %s. Could you try again?\n", p_choice);
                    continue;
                }

                String c_choice = rpsChoices.get(rand.nextInt(3));

                String result;
                if (( p_choice.equals("rock") && c_choice.equals("scissors") ) ||
                    ( p_choice.equals("scissors") && c_choice.equals("paper") ) ||
                    ( p_choice.equals("paper") && c_choice.equals("rock") )) {
                    result = "Human wins!";
                    humanScore++;
                } else if (p_choice.equals(c_choice)) {
                    result = "It's a tie";
                } else {
                    result = "Computer wins!";
                    computerScore++;
                }

                System.out.printf("Human chose %s, computer chose %s. "+result+"\n", p_choice, c_choice);
                System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);

                roundCounter++;
                keepplaying=readInput("Do you wish to continue playing? (y/n)?");
            }
        System.out.println("Bye bye :)");
    }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
